require 'httparty'
require 'nokogiri'
require 'json'

$url  = 'https://en.wikipedia.org'
$out  = 'infoboxes.json'
$uris = []
$obj  = []
$key  = nil
$val  = nil
$cut  = nil
ruby  = '/wiki/Ruby_(programming_language)'

def init_scrape uri
  puts "#{$obj.length + 1} => #{uri}"
  here = $url + uri
  get_this = '.infobox *'
  process scrape here, get_this
  continue?
end

def continue?
  return nil if $uris.length == 0
  $uris.each do |uri|
    reset
    init_scrape uri unless scraped.include? uri
  end
end

def scraped
  return $obj.map{|e| e[:uri]}
end

def scrape url, rule
  result = HTTParty.get(url).body
  return {:nodes => Nokogiri::HTML(result).css(rule), :url => url}
end

def process all
  $cut = {:uri => all[:url].gsub($url, ''), :timestamp => Time.now.to_i}
  all[:nodes].each { |raw| chop raw }
  save $obj.push $cut
end

def chop piece
  case piece.name
  when 'caption'
    get_lang piece
  when 'tr'
    get_data piece
  end
end

def get_lang node
  name = node.content
  $cut[:lang] = name
end

def get_data node
  key = node.css('th').text.strip
  value = node.css('td').text.strip
  a = key.length > 0
  b = value.length > 0
  a && b ? store(key, value) : a || b ? wait(key, value, node) : nil
end

def wait key, val, e
  $key = key != '' ? key : $key
  $val = val != '' ? val : $val
  continue e if $key && $val
end

def continue e
  store $key, get_uris($key, e), false
  reset
end

def reset
  $key = nil
  $val = nil
end

def get_uris k, e
  return nil unless k.downcase =~ /influenced/
  nodes = e.css('a')
  langs = []
  nodes.each do |a|
    href = a.attribute('href').to_s
    if href =~ /wiki/
      save_uri? href
      langs.push href
    end
  end
  return langs
end

def save_uri? uri
  $uris.push uri unless $uris.include? uri
end

def store key, val, bool = true
  value = bool ? shape(val) : val
  $cut[forge key] = value
end

def forge e
  return e.downcase.gsub(/(\s| )+/, '_').strip.to_sym
end

def shape e
  separator = /\s*(,|:)\s*/
  return e.split(separator)
          .map{ |part| clean part }
          .reject{ |part| part =~ separator }
end

def clean e
  reference = /\[\S+\]/
  clause = /\(.+\)/
  words = /\s*(or|and)\s+/
  more = /\s*;.*$/
  spaces = /(\s| )+/
  quotes = /"/
  return e.gsub(reference, '').gsub(clause, '').gsub(words, '')
          .gsub(more, '').gsub(spaces, ' ').gsub(quotes, '').strip
end

def save content
  File.open($out, 'w'){ |f| f.write JSON.pretty_generate content }
end

init_scrape ruby
