require 'json'

$hash = JSON.parse File.read 'infoboxes_clean.json'
$out  = 'infoboxes_linked.json'

def cure from = 'influenced_by'
  to = from == 'influenced_by' ? 'influenced' : 'influenced_by'
  $hash.each do |e|
    e.each do |k, v|
      v.each do |to_lang|
        add_lang e['lang'], to_lang, to
      end if k == from
    end if e
  end
end

def add_lang f_name, t_name, t_key
  to = $hash.index{|e| e['lang'] == t_name if e }
  $hash[to][t_key].push f_name
end

def remake
  uniq
  get_lengths
end

def uniq
  $hash.map!{|e| e ? {
    :lang => e['lang'],
    :influenced_by => replace(e['influenced_by']),
    :influenced => replace(e['influenced'])
  } : nil}
end

def get_lengths
  $hash.map!{|e| e ? {
    :lang => e[:lang],
    :length_influenced_by => replace_i(e[:influenced_by]),
    :influenced_by => replace(e[:influenced_by]),
    :length_influenced => replace_i(e[:influenced]),
    :influenced => replace(e[:influenced]),
    :length_influence => get_influence(e ,:influenced_by, :influenced)
  } : nil}
end

def replace e, default = nil, uniq = true
  return e ? uniq ? e.uniq.sort : e.length : default
end

def replace_i e
  return replace e, 0, false
end

def get_influence e, k1, k2
  return replace_i(e[k1]) + replace_i(e[k2])
end

def get_unlinked
  print "=> UNLINKED: "
  puts $hash.reject{|e| e && e[:length_influence] != 0}.compact.length
end

def save
  File.open($out, 'w'){|f| f.write JSON.pretty_generate $hash}
end

cure 'influenced'
cure
remake
get_unlinked
save
